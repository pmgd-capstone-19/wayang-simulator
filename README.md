# Wayang Simulator



## PMGD 2022 Capstone Project
##### Tim 19 - Level Up  
##### Serious Game

## Anggota Tim

| Stream               | Nama Lengkap                    | Asal Universitas                           |
|----------------------|---------------------------------|--------------------------------------------|
| Mentor               | Amir Hasnudin Fauzi, S.T., M.T. | Universitas Telkom                         |
| Game Project Manager | Ikhsan                          | Universitas Muhammadiyah Sidenreng Rappang |
|                      | Ratna Dewi Purnamasari          | Universitas Telkom                         |
| Game Artist          | Faiz Dzakwan Naufal             | Universitas Negeri Makassar                |
|                      | Rafiqah Kurniati                | Universitas Bima Darma                     |
| Game Designer        | Muhammad Lutfi Abdul Aziz       | Universitas Pendidikan Indonesia           |
|                      | Nurhaliza Wirdayani             | Universitas Muhammadiyah Bone              |
| Game Programmer      | Juan Audrica Naufaldy           | Universitas Diponegoro                     |
|                      | Mohammad Rifa'i                 | Institut Teknologi Telkom Purwokerto       |
|                      | Wahidah Priscillah              | Universitas Samawa                         |
|                      | Rafy Irfanza                    | Institut Teknologi Telkom Purwokerto       |
