using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DragObject : MonoBehaviour
{
    private Vector3 mOffset;
    private float mZCoord;

    void OnMouseDown()
    {
        if (!GameManager.Instance.isGamePaused())
        {
            mZCoord = Camera.main.WorldToScreenPoint(gameObject.transform.position).z;

            // Store offset = gameobject world pos - mouse world pos
            mOffset = gameObject.transform.position - GetMouseWorldPos();
        }
    }

    private Vector3 GetMouseWorldPos()
    {
        // Pixel coordinate (x,y)
        Vector3 mousePoint = Input.mousePosition;

        // Z coordinate of the game object on screen
        mousePoint.z = mZCoord;

        return Camera.main.ScreenToWorldPoint(mousePoint);
    }

    void OnMouseDrag()
    {
        if (!GameManager.Instance.isGamePaused())
        {
            transform.position = GetMouseWorldPos() + mOffset;
        }
    }
}
