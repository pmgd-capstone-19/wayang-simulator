using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Target : MonoBehaviour
{
    public ArmJoint[] joints;
    private float radius;
    private Vector3 circleCenter;

    // Start is called before the first frame update
    void Start()
    {
        radius = Vector3.Distance(joints[0].transform.position, joints[1].transform.position) + Vector3.Distance(joints[1].transform.position, joints[2].transform.position) + 0.1f;
    }

    // Update is called once per frame
    void Update()
    {
        circleCenter = joints[0].transform.position;

        float currDistance = Vector3.Distance(joints[0].transform.position, joints[1].transform.position) + Vector3.Distance(joints[1].transform.position, transform.position);
        if (currDistance > radius)
        {
            Vector3 fromOrigintoObject = transform.position - circleCenter;
            fromOrigintoObject *= radius / currDistance;
            transform.position = circleCenter + fromOrigintoObject;
        }
    }

    private float GetDistance(Vector3 pos1, Vector3 pos2, Vector3 pos3)
    {
        return Vector3.Distance(joints[0].transform.position, joints[1].transform.position) + Vector3.Distance(joints[1].transform.position, transform.position);
    }
}
