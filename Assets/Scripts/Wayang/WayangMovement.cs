using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WayangMovement : MonoBehaviour
{
    public float moveSpeed;

    [SerializeField]
    private float rotationTime = 0.5f;


    private bool isRotating = false;

    void Update()
    {
        if (UnitSelections.Instance.unitSelected.Contains(this.gameObject))
        {
            // Ngambil variabel dari axing yang sudah disetting di unity input dengan output (-1,1)
            float v = Input.GetAxis("Vertical");
            float h = Input.GetAxis("Horizontal");
            
            if (!isRotating)
            {
                if (transform.eulerAngles.y > 90f)
                {
                    transform.Translate(new Vector3(-h, 0, -v) * moveSpeed);
                }
                else
                {
                    transform.Translate(new Vector3(h, 0, -v) * moveSpeed);
                }
            }

            // Agar tidak keluar batas atas
            if (transform.position.y > 6f)
            {
                transform.position = new Vector3(transform.position.x, 6f, transform.position.z);
            }

            // Agar tidak keluar batas bawah
            if (transform.position.y < -7f)
            {
                transform.position = new Vector3(transform.position.x, -7f, transform.position.z);
            }
            
            // Agar tidak keluar batas kiri
            if (transform.position.x > 10f)
            {
                transform.position = new Vector3(10f, transform.position.y, transform.position.z);
            }

            // Agar tidak keluar batas kanan
            if (transform.position.x < -10f)
            {
                transform.position = new Vector3(-10f, transform.position.y, transform.position.z);
            }

            if (Input.GetKeyDown(KeyCode.E) && !isRotating)
            {
                isRotating = true;
                StartCoroutine(Rotate(Vector3.forward, 180, rotationTime));
            }

            if (Input.GetKeyDown(KeyCode.Q) && !isRotating)
            {
                isRotating = true;
                StartCoroutine(Rotate(Vector3.forward, -180, rotationTime));
            }
        }
    }

    IEnumerator Rotate(Vector3 axis, float angle, float duration = 1.0f)
    {
        Quaternion from = transform.rotation;
        Quaternion to = transform.rotation;
        to *= Quaternion.Euler(axis * angle);

        float elapsed = 0.0f;
        while (elapsed < duration)
        {
            transform.rotation = Quaternion.Slerp(from, to, elapsed / duration);
            elapsed += Time.deltaTime;
            yield return null;
        }
        transform.rotation = to;
        isRotating = false;
    }
}
