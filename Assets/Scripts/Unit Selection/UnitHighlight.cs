using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitHighlight : MonoBehaviour
{
    private int clickableMask;
    private int selectedMask;

    private void Awake()
    {
        clickableMask = LayerMask.NameToLayer("Clickable");
        selectedMask = LayerMask.NameToLayer("Selected");
    }

    // Update is called once per frame
    void Update()
    {
        if (UnitSelections.Instance.unitSelected.Contains(this.gameObject))
        {
            foreach (Transform g in transform.GetComponentsInChildren<Transform>())
            {
                g.gameObject.layer = selectedMask;
            }
        }
        else
        {
            foreach (Transform g in transform.GetComponentsInChildren<Transform>())
            {
                g.gameObject.layer = clickableMask;
            }
        }
    }
}
