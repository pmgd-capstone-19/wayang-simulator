using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitClick : MonoBehaviour
{
    private Camera myCam;

    public LayerMask clickable;
    public LayerMask selected;
    public LayerMask background;
    public LayerMask button;

    // Start is called before the first frame update
    void Start()
    {
        myCam = Camera.main;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0) && !GameManager.Instance.isGamePaused())
        {
            RaycastHit hit;
            Ray ray = myCam.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out hit, Mathf.Infinity, clickable | selected))
            {
                // If we click a clickable object
                if (Input.GetKey(KeyCode.LeftShift))
                {
                    // Shift held
                    UnitSelections.Instance.ShiftClickSelect(hit.collider.transform.root.gameObject);
                }
                else
                {
                    // Normal click
                    UnitSelections.Instance.ClickSelect(hit.collider.transform.root.gameObject);
                }
            }
            else if (Physics.Raycast(ray, Mathf.Infinity, background))
            {
                // If we don't click a clickable object and we are not holding Lshift
                if (!Input.GetKey(KeyCode.LeftShift))
                {
                    UnitSelections.Instance.DeselectAll();
                }
            }
        }
    }
}
