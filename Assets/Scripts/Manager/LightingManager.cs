using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LightingManager : MonoBehaviour
{
    public static LightingManager instance { get; private set; }

    public GameObject lightSource1;
    public GameObject lightSource2;
    public GameObject lightingButton;

    [SerializeField]
    private float transitionTime = 1f;
    [SerializeField]
    private float angleLight1 = -85; //365
    [SerializeField]
    private float angleLight2 = 40f; //-230
    [SerializeField]
    private Material highlightMaterial;

    private bool isMoving = false;
    private bool isOn = false;

    private void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(gameObject);
        }
        else
        {
            instance = this;
        }
    }

    private void Start()
    {
        lightSource1.transform.rotation = Quaternion.Euler(450, 0, 0);
        lightSource2.transform.rotation = Quaternion.Euler(-270, 0, 0);
        lightingButton.transform.localPosition = new Vector3(0, 540, 0);
        BlinkingObject(lightingButton);
    }

    public void SwitchLight()
    {
        if (!isMoving)
        {
            isMoving = true;
            StartCoroutine(Switch(transitionTime));
        }
    }

    IEnumerator Switch(float duration = 1.0f)
    {
        Vector3 axis = Vector3.right;
        Vector3 currButtonPos = lightingButton.transform.position;
        Vector3 button_to;

        // Light source 1
        Quaternion light1_from = lightSource1.transform.rotation;
        Quaternion light1_to = lightSource1.transform.rotation;

        // Light source 2
        Quaternion light2_from = lightSource2.transform.rotation;
        Quaternion light2_to = lightSource2.transform.rotation;

        if (!isOn)
        {
            light1_to *= Quaternion.Euler(axis * angleLight1);
            light2_to *= Quaternion.Euler(axis * angleLight2);
            button_to = new Vector3(0, 4f, 0);

            isOn = true;
        }
        else
        {
            light1_to *= Quaternion.Euler(axis * -angleLight1);
            light2_to *= Quaternion.Euler(axis * -angleLight2);
            button_to = new Vector3(0, 5.8f, 0);

            isOn = false;
        }

        float elapsed = 0.0f;
        while (elapsed < duration)
        {
            lightSource1.transform.rotation = Quaternion.Slerp(light1_from, light1_to, elapsed);
            lightSource2.transform.rotation = Quaternion.Slerp(light2_from, light2_to, elapsed);
            lightingButton.transform.position = Vector3.Lerp(currButtonPos, button_to, elapsed);
            elapsed += Time.deltaTime / duration;
            yield return null;
        }

        lightSource1.transform.rotation = light1_to;
        lightSource2.transform.rotation = light2_to;
        lightingButton.transform.position = button_to;
        isMoving = false;
    }

    public void BlinkingObject(GameObject obj)
    {
        StartCoroutine(Blinking(obj));
    }

    private IEnumerator Blinking(GameObject obj)
    {
        while (!isOn || GameManager.Instance.IsGameOver())
        {
            yield return new WaitForSeconds(1f);
            obj.GetComponent<Image>().material = highlightMaterial;
            yield return new WaitForSeconds(1f);
            obj.GetComponent<Image>().material = null;
        }
    }
}
