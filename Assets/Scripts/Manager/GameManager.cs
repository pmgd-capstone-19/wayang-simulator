using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance { get; private set; }

    public DialogueController tutorial;
    public TimelinePlayer timelinePlayer;
    public AudioClip narration;
    public GameObject timerPanel;
    public TextMeshProUGUI timerTxt; 

    public UnityEvent OnGameOver;

    private bool isGameOver = false;
    private bool hasTheGameStarted = false;
    private float timer;

    private void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(gameObject);
        }
        else
        {
            Instance = this;
        }

        PauseGame();
    }

    private void Start()
    {
        tutorial.StartDialogue();
        timer = narration.length;
    }

    private void Update()
    {
        if (hasTheGameStarted)
        {
            SetTimer();
        }
    }

    public void StartGame()
    {
        if (!hasTheGameStarted)
        {
            hasTheGameStarted = true;

            timerPanel.SetActive(true);
            timelinePlayer.StartTimeline();
            SoundManager.instance.FadeOutBGM(1f);
        }
    }

    public void ResumeGame()
    {
        Time.timeScale = 1;
    }

    public void PauseGame()
    {
        Time.timeScale = 0;
    }

    public bool isGamePaused()
    {
        return Time.timeScale == 0;
    }

    public void GameOver()
    {
        isGameOver = true;

        OnGameOver?.Invoke();
    }

    public bool IsGameOver()
    {
        return isGameOver;
    }

    public void RestartGame()
    {
        SceneManager.LoadScene("Sandbox");
    }

    public void BackToMainMenu()
    {
        SceneManager.LoadScene("MainMenu");
    }

    private void SetTimer()
    {
        timer = timer - Time.deltaTime > 0 ? timer - Time.deltaTime : 0;

        float minutes = Mathf.FloorToInt(timer / 60);
        float seconds = Mathf.FloorToInt(timer % 60);
        timerTxt.text = string.Format("{0:00}:{1:00}", minutes, seconds);
    }
}
