using System.Collections;
using UnityEngine;
using UnityEngine.Events;

public class DialogueController : MonoBehaviour
{
    public Dialogue dialogue;

    public GameObject speakerLeft;
    public GameObject speakerRight;

    public float textSpeed;

    public UnityEvent OnDialogueEnded;

    private SpeakerUIController speakerUILeft;
    private SpeakerUIController speakerUIRight;

    private SpeakerUIController activeSpeakerUI;

    private int activeLineIndex = 0;
    private bool isDialogueActive = false;

    // Start is called before the first frame update
    void Awake()
    {
        speakerUILeft = speakerLeft.GetComponent<SpeakerUIController>();
        speakerUIRight = speakerRight.GetComponent<SpeakerUIController>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0) && isDialogueActive)
        {
            if (activeSpeakerUI.Dialogue == dialogue.lines[activeLineIndex].text)
            {
                AdvanceDialogue();
            }
            else
            {
                StopAllCoroutines();
                activeSpeakerUI.Dialogue = dialogue.lines[activeLineIndex].text;
            }
        }
    }

    private void AdvanceDialogue()
    {
        if (activeLineIndex < dialogue.lines.Length - 1)
        {
            activeLineIndex++;
            activeSpeakerUI.Dialogue = string.Empty;
            DisplayLine();
        }
        else
        {
            speakerUILeft.Hide();
            speakerUIRight.Hide();
            activeLineIndex = 0;
            EndDialogue();
        }
    }

    private void DisplayLine()
    {
        Line line = dialogue.lines[activeLineIndex];
        Character character = line.character;

        if (line.dialogueSide.ToString() == "Left")
        {
            activeSpeakerUI = speakerUILeft;
            SetDialogue(speakerUILeft, speakerUIRight, line);
        }
        else
        {
            activeSpeakerUI = speakerUIRight;
            SetDialogue(speakerUIRight, speakerUILeft, line);
        }
    }

    private void SetDialogue(SpeakerUIController activeSpeakerUI, SpeakerUIController inactiveSpeakerUI, Line line)
    {
        activeSpeakerUI.Speaker = line.character;
        activeSpeakerUI.Mood = line.mood;

        StartCoroutine(TypeLine());
        
        activeSpeakerUI.Show();
        inactiveSpeakerUI.Hide();
    }

    private IEnumerator TypeLine()
    {
        foreach (char c in dialogue.lines[activeLineIndex].text.ToCharArray())
        {
            activeSpeakerUI.Dialogue += c;
            yield return new WaitForSecondsRealtime(textSpeed);
        }
    }

    public void StartDialogue()
    {
        isDialogueActive = true;

        speakerUILeft.Dialogue = string.Empty;
        speakerUIRight.Dialogue = string.Empty;
        DisplayLine();
    }

    private void EndDialogue()
    {
        isDialogueActive = false;
        OnDialogueEnded?.Invoke();
    }
}
