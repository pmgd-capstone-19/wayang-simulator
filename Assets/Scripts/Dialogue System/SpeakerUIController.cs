using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class SpeakerUIController : MonoBehaviour
{
    public Image portrait;
    public TextMeshProUGUI fullName;
    public TextMeshProUGUI _dialogue;
    public Mood mood;

    private Character _speaker;
    public Character Speaker
    {
        get 
        { 
            return _speaker; 
        }
        set 
        { 
            _speaker = value;
            portrait.sprite = _speaker.portraitDefault;
            fullName.text = _speaker.fullName;
        }
    }

    public string Dialogue
    {
        get
        {
            return _dialogue.text;
        }
        set
        { 
            _dialogue.text = value;
        }
    }

    public Mood Mood
    {
        set
        {
            Sprite sprite;
            
            if (value == Mood.SenyumRamah)
            {
                sprite = _speaker.portraitSenyumRamah;
            }
            else if (value == Mood.CukupSerius)
            {
                sprite = _speaker.portraitCukupSerius;
            }
            else if (value == Mood.SenyumOptimis)
            {
                sprite = _speaker.portraitSenyumOptimis;
            }
            else if (value == Mood.Tertawa)
            {
                sprite = _speaker.portraitTertawa;
            }
            else
            {
                sprite = _speaker.portraitDefault;
            }

            portrait.sprite = sprite;
        }
    }

    public bool HasSpeaker()
    {
        return _speaker != null;
    }

    public bool IsSpeaker(Character character)
    {
        return _speaker == character;
    }

    public void Show()
    {
        gameObject.SetActive(true);
    }

    public void Hide()
    {
        gameObject.SetActive(false);
    }
}
