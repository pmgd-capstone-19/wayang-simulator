using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadSceneDelay : MonoBehaviour
{
    [Header("Main Settings")]
    public string scene;
    public float Delay;

    void LoadScene()
    {
        SceneManager.LoadScene(scene);
    }

    // Start is called before the first frame update
    void Start()
    {
        Invoke("LoadScene", Delay);
    }

    // Update is called once per frame
    void Update()
    {

    }
}
