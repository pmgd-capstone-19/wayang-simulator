using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FadeOut : MonoBehaviour
{
    [Header("Settings")]
    public Image image;
    public float FadeSpeed;
    public float Delay;
    bool StartFadeOut = false;

    void Awake()
    {
        image.rectTransform.localScale = new Vector2(Screen.width, Screen.height);
        image.enabled = false;
        image.color = Color.clear;
    }

    void FadeOutFunction()
    {
        image.color = Color.Lerp(image.color, Color.black, FadeSpeed * Time.deltaTime);
    }
    // Start is called before the first frame update
    void Start()
    {
        Invoke("SetTrue_StartFadeOut", Delay);
    }


    void SetTrue_StartFadeOut()
    {
        StartFadeOut = true;
        image.enabled = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (StartFadeOut)
        {
            FadeOutFunction();
        }
    }
}
