using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    [Header("Main Menu Panel List")]
    public GameObject MainPanel;
    public GameObject PengaturanPanel;
    public GameObject GalleryPanel;
    public GameObject CustomizationPanel;
    public GameObject Display;
    public GameObject Suara;
    public GameObject Achievement;
    public GameObject Hasil;

    // Start is called before the first frame update
    void Start()
    {
        Time.timeScale = 1;

        MainPanel.SetActive(true);
        PengaturanPanel.SetActive(false);
        GalleryPanel.SetActive(false);
        CustomizationPanel.SetActive(false);
        Display.SetActive(true);
        Suara.SetActive(false);
        Achievement.SetActive(false);
        Hasil.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void DisplayButton()
    {
        MainPanel.SetActive(true);
        PengaturanPanel.SetActive(true);
        GalleryPanel.SetActive(false);
        CustomizationPanel.SetActive(false);
        Display.SetActive(true);
        Suara.SetActive(false);
        Achievement.SetActive(false);
        Hasil.SetActive(false);
    }
    public void SuaraButton()
    {
        MainPanel.SetActive(true);
        PengaturanPanel.SetActive(true);
        GalleryPanel.SetActive(false);
        CustomizationPanel.SetActive(false);
        Display.SetActive(false);
        Suara.SetActive(true);
        Achievement.SetActive(false);
        Hasil.SetActive(false);
    }
    public void AchievementButton()
    {
        MainPanel.SetActive(true);
        PengaturanPanel.SetActive(true);
        GalleryPanel.SetActive(false);
        CustomizationPanel.SetActive(false);
        Display.SetActive(false);
        Suara.SetActive(false);
        Achievement.SetActive(true);
        Hasil.SetActive(false);
    }
    public void HasilButton()
    {
        MainPanel.SetActive(true);
        PengaturanPanel.SetActive(true);
        GalleryPanel.SetActive(false);
        CustomizationPanel.SetActive(false);
        Display.SetActive(false);
        Suara.SetActive(false);
        Achievement.SetActive(false);
        Hasil.SetActive(true);
    }
    public void BackButton()
    {
        MainPanel.SetActive(true);
        PengaturanPanel.SetActive(false);
        GalleryPanel.SetActive(false);
        CustomizationPanel.SetActive(false);
        Display.SetActive(false);
        Suara.SetActive(false);
        Achievement.SetActive(false);
        Hasil.SetActive(false);
    }
    public void GalleryButton()
    {
        MainPanel.SetActive(true);
        PengaturanPanel.SetActive(false);
        GalleryPanel.SetActive(true);
        CustomizationPanel.SetActive(false);
        Display.SetActive(false);
        Suara.SetActive(false);
        Achievement.SetActive(false);
        Hasil.SetActive(false);
    }
    public void CustomizationButton()
    {
        MainPanel.SetActive(true);
        PengaturanPanel.SetActive(false);
        GalleryPanel.SetActive(false);
        CustomizationPanel.SetActive(true);
        Display.SetActive(false);
        Suara.SetActive(false);
        Achievement.SetActive(false);
        Hasil.SetActive(false);
    }
    public void ExitGame()
    {
        Application.Quit();
    }
}
