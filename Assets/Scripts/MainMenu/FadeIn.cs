using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FadeIn : MonoBehaviour
{
    [Header("Settings")]
    public Image image;
    public float FadeSpeed;

    void Awake()
    {
        image.rectTransform.localScale = new Vector2(Screen.width, Screen.height);
    }

    void FadeInFunction()
    {
        image.color = Color.Lerp(image.color, Color.clear, FadeSpeed * Time.deltaTime);
    }
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        FadeInFunction();
    }
}
