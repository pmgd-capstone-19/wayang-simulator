using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

public class TimelinePlayer : MonoBehaviour
{
    private PlayableDirector director;
    public GameObject controlPanel;

    // Start is called before the first frame update
    private void Awake()
    {
        director = GetComponent<PlayableDirector>();
        director.played += DirectorPlayed;
        director.stopped += DirectorStopped;
    }

    private void DirectorPlayed(PlayableDirector obj)
    {
        //controlPanel.SetActive(false);
    }

    private void DirectorStopped(PlayableDirector obj)
    {
        //controlPanel.SetActive(true);
        GameManager.Instance.GameOver();
    }

    public void StartTimeline()
    {
        StartCoroutine(DelayedTimeline());
    }

    private IEnumerator DelayedTimeline()
    {
        yield return new WaitForSecondsRealtime(3f);
        director.Play();
    }
}
