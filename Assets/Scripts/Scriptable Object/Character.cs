using UnityEngine;

[CreateAssetMenu(fileName = "New Character", menuName = "Character")]
public class Character : ScriptableObject
{
    public string fullName;
    public Sprite portraitDefault;
    public Sprite portraitSenyumRamah;
    public Sprite portraitCukupSerius;
    public Sprite portraitSenyumOptimis;
    public Sprite portraitTertawa;
}
