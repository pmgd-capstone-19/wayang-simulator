using UnityEngine;

public enum DialogueSide { Left, Right }
public enum Mood { Default, SenyumRamah, CukupSerius, SenyumOptimis, Tertawa }

[System.Serializable]
public struct Line
{
    public DialogueSide dialogueSide;
    public Character character;
    public Mood mood;

    [TextArea(2, 5)]
    public string text;
}

[CreateAssetMenu(fileName = "New Dialogue", menuName = "Dialogue")]
public class Dialogue : ScriptableObject
{
    public Line[] lines;
}
